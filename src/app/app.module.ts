import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MessageListComponent } from './message-list/message-list.component';
import { from } from 'rxjs';
import { HttpClientModule } from '@angular/common/http';

import {
  OktaAuthModule,
  OktaCallbackComponent,
} from '@okta/okta-angular';

const appRoutes: Routes = [
  { path: 'implicit/callback', component: OktaCallbackComponent },
  { path: 'message_list', component: MessageListComponent },
]

const config = {
  issuer: 'https://dev-652307.okta.com/oauth2/default',
  redirectUri: 'http://localhost:4200/implicit/callback',
  clientId: '0oa2e5ndzitFnWpQs357',
  pkce: true
}

@NgModule({
  declarations: [
    AppComponent,
    MessageListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    OktaAuthModule.initAuth(config),
    RouterModule.forRoot(appRoutes),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
