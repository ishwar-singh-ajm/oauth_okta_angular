import { Component, OnInit, Injectable } from '@angular/core';
// import { Http, Headers, RequestOptions } from '@angular/http';
import { OktaAuthService } from '@okta/okta-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

interface Message {
  date: String,
  text: String
}

@Injectable()

@Component({
  template: `
   <div *ngIf="messages.length">
     <li *ngFor="let message of messages">{{message.message}}</li>
   </div>
 `
})

export class MessageListComponent implements OnInit {
  messages: Array<Message>[];
  isAuthenticated: boolean;
  constructor(private oktaAuth: OktaAuthService, private http: HttpClient, private router: Router) {
    this.messages = [];
    // Subscribe to authentication state changes
    this.oktaAuth.$authenticationState.subscribe(
      (isAuthenticated: boolean) => this.isAuthenticated = isAuthenticated
    );
  }

  async ngOnInit() {

    this.isAuthenticated = await this.oktaAuth.isAuthenticated();

    if (this.isAuthenticated == false) {
      this.router.navigate(["/"]);
      return;
    }

    console.log("this.isAuthenticated", this.isAuthenticated);

    const accessToken = await this.oktaAuth.getAccessToken();
    const userDetails = await this.oktaAuth.getUser();

    console.log("this.getUser ===> ", userDetails);

    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Authorization', 'Bearer ' + accessToken);

    console.log("headers ======> ", headers);

    // For SignIn with Google
    // {
    //   "web": {
    //     "client_id": "912473838901-u1rchge99l47mk2gnqtoa78hffgr89gu.apps.googleusercontent.com",
    //     "project_id": "oauth-nodejs-264110",
    //     "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    //     "token_uri": "https://oauth2.googleapis.com/token",
    //     "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    //     "client_secret": "eTzUNWLT4nt-GAqxsmLF4z0H",
    //     "redirect_uris": [
    //       "http://localhost:4200/implicit/callback",
    //       "http://localhost:4200"
    //     ],
    //     "javascript_origins": [
    //       "http://localhost:4200"
    //     ]
    //   }
    // }




    // Make request
    this.http.get<any>('http://localhost:3000/api/messages', { headers: headers, params: {} }).subscribe(
      (result) => {
        this.messages = result;
        console.log("Subscribe Data is ===>", result);
      }, (error) => {
        console.log("Subscribe Data Error is ===>", error);
      }
    );
  }
}
